'use client'

import { useEffect, useState } from 'react'
import { db } from '@/lib/firebase'
import { onSnapshot, doc } from '@firebase/firestore'
import Weather from '@/components/Weather'

interface ICurrentWeather {
  temperature: number
  humidity: number
  condition: string
  sunrise: string
  sunset: string
  pressure: number
  precipitation: number
  wind: number
}

interface ILocation {
  id: string
  name: string
  currentWeather: ICurrentWeather
}

export default function Home() {
  const [location, setLocation] = useState<ILocation | null>(null)

  useEffect(() => {
    const docRef = doc(db, 'Locations', 'HdCAIwdvVl3mgdJQ0kvi')
    const unsubscribe = onSnapshot(docRef, (docSnapshot) => {
      if (docSnapshot.exists()) {
        const doc = docSnapshot.data() as ILocation
        setLocation({
          id: docSnapshot.id,
          name: doc.name,
          currentWeather: {
            temperature: doc.currentWeather.temperature,
            humidity: doc.currentWeather.humidity,
            condition: doc.currentWeather.condition,
            sunrise: doc.currentWeather.sunrise,
            sunset: doc.currentWeather.sunset,
            pressure: doc.currentWeather.pressure,
            precipitation: doc.currentWeather.precipitation,
            wind: doc.currentWeather.wind
          }
        })
      }
    })
    return () => unsubscribe()
  }, [])

  if (!location) {
    return <h1 className="h-screen flex justify-center items-center text-white text-2xl text-center">Loading ....</h1>
  }

  return <Weather data={location} />
}
