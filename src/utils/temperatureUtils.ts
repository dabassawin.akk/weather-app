export const celsiusToFahrenheit = (celsius: number | null): number => {
  return typeof celsius === 'number' ? Math.floor((celsius * 9) / 5 + 32) : NaN
}
