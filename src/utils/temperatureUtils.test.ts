import { describe, expect, it } from 'vitest'
import { celsiusToFahrenheit } from './temperatureUtils'

describe('celsiusToFahrenheit', () => {
  it('converts 0°C to 32°F', () => {
    expect(celsiusToFahrenheit(0)).toBe(32)
  })

  it('converts 100°C to 212°F', () => {
    expect(celsiusToFahrenheit(100)).toBe(212)
  })

  it('converts -40°C to -40°F', () => {
    expect(celsiusToFahrenheit(-40)).toBe(-40)
  })

  it('returns NaN for null input', () => {
    expect(celsiusToFahrenheit(null)).toBeNaN()
  })

  it('returns NaN for non-number input', () => {
    expect(celsiusToFahrenheit(NaN)).toBeNaN()
  })
})
