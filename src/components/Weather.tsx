'use client'

import Image from 'next/image'
import partlyCloudySvg from '../../public/images/partly-cloudy-day.svg'
import { useMemo, useState } from 'react'
import { celsiusToFahrenheit } from '@/utils/temperatureUtils'

interface ICurrentWeather {
  temperature: number
  humidity: number
  condition: string
  sunrise: string
  sunset: string
  pressure: number
  precipitation: number
  wind: number
}

interface IProps {
  data: {
    id: string
    name: string
    currentWeather: ICurrentWeather
  }
}

export default function Weather({ data }: IProps) {
  const [isCelsius, setIsCelsius] = useState<boolean>(true)

  const temperature = useMemo(() => {
    const temp = data.currentWeather.temperature ?? null
    return isCelsius ? temp : celsiusToFahrenheit(temp)
  }, [isCelsius, data.currentWeather.temperature])

  return (
    <div className="h-screen flex justify-center items-center font-roboto text-3xl text-white select-none">
      <div className="w-4/5 2xl:w-2/3 p-10 rounded-lg border-2 border-white border-opacity-40">
        <div id="header">
          <div className="text-4xl font-bold" data-testid="locationName">
            {data.name}
          </div>
        </div>
        <div id="currentTemperature" className="grid grid-cols-1 2xl:grid-cols-2 mt-5">
          <div className="grid grid-cols-2 items-center gap-5 mx-auto">
            <Image
              src={partlyCloudySvg}
              alt="partly-cloudy-day"
              className="text-white ml-auto"
              width={200}
              height={200}
            />
            <div>
              <div className="flex">
                <div className="text-8xl" data-testid="temperature">
                  {temperature}
                </div>
                <div
                  className="text-6xl cursor-pointer"
                  data-testid="toggleUnitButton"
                  onClick={() => setIsCelsius(!isCelsius)}>
                  {isCelsius ? '°C' : '°F'}
                </div>
              </div>
              <div className="text-nowrap text-2xl" data-testid="condition">
                {data.currentWeather.condition}
              </div>
            </div>
          </div>
          <div className="border-l-0 2xl:border-l-4 border-white border-opacity-20 pt-10 2xl:pt-0 flex items-center">
            <div className="grid grid-cols-2 lg:grid-cols-3 gap-10 flex-1 px-10">
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl" data-testid="humidity">
                  {data.currentWeather.humidity}%
                </div>
                <div className="text-xl opacity-80 font-light">Humidity</div>
              </div>
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl" data-testid="wind">
                  {data.currentWeather.wind} km/h
                </div>
                <div className="text-xl opacity-80 font-light">Wind</div>
              </div>
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl" data-testid="sunrise">
                  {data.currentWeather.sunrise}
                </div>
                <div className="text-xl opacity-80 font-light">Sunrise</div>
              </div>
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl text-nowrap" data-testid="pressure">
                  {data.currentWeather.pressure} hPa
                </div>
                <div className="text-xl opacity-80 font-light">Pressure</div>
              </div>
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl" data-testid="precipitation">
                  {data.currentWeather.precipitation}%
                </div>
                <div className="text-xl opacity-80 font-light">Precipitation</div>
              </div>
              <div className="text-center flex flex-col gap-2">
                <div className="font-bold text-3xl" data-testid="sunset">
                  {data.currentWeather.sunset}
                </div>
                <div className="text-xl opacity-80 font-light">Sunset</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
