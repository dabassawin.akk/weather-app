import { afterEach, describe, expect, test } from 'vitest'
import { cleanup, fireEvent, render, screen } from '@testing-library/react'
import Weather from './Weather'

describe('Weather Component', () => {
  afterEach(() => {
    cleanup()
  })

  const weatherData = {
    id: 'HdCAIwdvVl3mgdJQ0kvi',
    name: 'Bangkok, Thailand',
    currentWeather: {
      temperature: 31,
      humidity: 63,
      condition: 'Mostly sunny',
      sunrise: '05:07',
      sunset: '18:57',
      pressure: 1012,
      precipitation: 5,
      wind: 7
    }
  }

  test('renders location name', () => {
    render(<Weather data={weatherData} />)
    expect(screen.getByTestId('locationName').textContent).toBe('Bangkok, Thailand')
  })

  test('renders current weather details', () => {
    render(<Weather data={weatherData} />)
    expect(screen.getByTestId('condition').textContent).toBe('Mostly sunny')
    expect(screen.getByTestId('humidity').textContent).toBe('63%')
    expect(screen.getByTestId('temperature').textContent).toBe('31')
    expect(screen.getByTestId('sunrise').textContent).toBe('05:07')
    expect(screen.getByTestId('sunset').textContent).toBe('18:57')
    expect(screen.getByTestId('pressure').textContent).toBe('1012 hPa')
    expect(screen.getByTestId('precipitation').textContent).toBe('5%')
    expect(screen.getByTestId('wind').textContent).toBe('7 km/h')
  })

  test('toggles temperature unit between Celsius and Fahrenheit', () => {
    render(<Weather data={weatherData} />)
    const temperatureElement = screen.getByTestId('temperature')
    const toggleUnitButton = screen.getByTestId('toggleUnitButton')
    // Initially in Celsius
    expect(temperatureElement.textContent).toBe('31')
    expect(toggleUnitButton.textContent).toBe('°C')
    // Toggle to Fahrenheit
    fireEvent.click(toggleUnitButton)
    expect(temperatureElement.textContent).toBe('87')
    expect(toggleUnitButton.textContent).toBe('°F')
    // Toggle back to Celsius
    fireEvent.click(toggleUnitButton)
    expect(temperatureElement.textContent).toBe('31')
    expect(toggleUnitButton.textContent).toBe('°C')
  })
})
